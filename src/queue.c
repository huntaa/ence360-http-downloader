#include "queue.h"

#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>

#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)

/*
 * a circular buffer fifo queue for threads to access items to process
 * 
 * @attr data - where the actual items are stored
 * @attr buf_size - size of the buffer
 * @attr head - head of the circular buffer
 * @attr tail - tail of the circular buffer
 * @attr tasks_full - semaphore initialized to the length of the queue
 * @attr tasks_empty - semaphore initialized to 0
 */
typedef struct QueueStruct {
    void **data;
    size_t buf_size;
    size_t head;
    size_t tail;
    sem_t tasks_full;
    sem_t tasks_empty;
    pthread_mutex_t lock;
} Queue;


/*
 * creates a new queue, allocates memory to it and initiailzes attributes
 * 
 * @param size - desired length of the queue
 * @return Queue - the queue created
 */
Queue *queue_alloc(int size) {
    Queue *queue = malloc(sizeof(Queue));
    queue->data = calloc(size, sizeof(void*));
    queue->buf_size = size;
    queue->head = 0;
    queue->tail = 0;

    sem_init(&queue->tasks_full, 0, size);
    sem_init(&queue->tasks_empty, 0, 0);
    return queue;
}

/*
 *free the memory allocated to the queue by malloc
 *
 * @param queue - the queue we wish to free
 */
void queue_free(Queue *queue) {
    free(queue->data);
    free(queue);
}


/*
 * puts a new item on the queue
 * 
 * @param queue - the that we want to add the item to
 * @param item - the item we wan to add to the queue
 */
void queue_put(Queue *queue, void *item) {
    sem_wait(&queue->tasks_full);
    pthread_mutex_lock(&queue->lock);

    queue->data[queue->tail] = item;
    queue->tail = (queue->tail + 1) % queue->buf_size;
    sem_post(&queue->tasks_empty);
    pthread_mutex_unlock(&queue->lock);
}


/*
 * gets an item from the front of the queue, and returns it
 * 
 * @param queue - the queue to get the item from
 * @return void* - a pointer to the item retrieved
 */
void *queue_get(Queue *queue) {
    printf("entered queue\n");
    sem_wait(&queue->tasks_empty);
    pthread_mutex_lock(&queue->lock);
        void *item = queue->data[queue->head];
        queue->head = (queue->head + 1) % queue->buf_size;
        sem_post(&queue->tasks_full);
    pthread_mutex_unlock(&queue->lock);
    return item;
}