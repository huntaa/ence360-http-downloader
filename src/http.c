#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>

#include "http.h"

#define BUF_SIZE 1024
#define RESPONSE_BUF_SIZE 1024




/**
 * Perform an HTTP 1.0 query to a given host and page and port number.
 * host is a hostname and page is a path on the remote server. The query
 * will attempt to retrievev content in the given byte range.
 * User is responsible for freeing the memory.
 * 
 * @param host - The host name e.g. www.canterbury.ac.nz
 * @param page - e.g. /index.html
 * @param range - Byte range e.g. 0-500. NOTE: A server may not respect this
 * @param port - e.g. 80
 * @return Buffer - Pointer to a buffer holding response data from query
 *                  NULL is returned on failure.
 */
const char * GET_REQUEST_FORMAT =  "GET /%s HTTP/1.0\r\n"
                            "Host: %s\r\n"
                            "Range: bytes=%s\r\n"
                            "User-Agent: getter\r\n\r\n";

const char * HEAD_REQUEST_FORMAT = "HEAD /%s HTTP/1.0\r\n"
                            "Host: %s\r\n"
                            "User-Agent: getter\r\n\r\n";

enum request_type {GET, HEAD};


/*
 * Creates a sockect, connects it to the host and returns it.
 *  
 * @param host - the host name of the server to connect to
 * @param port - the port to connect to
 * @return int - the file descriptor of the socket created
 */
int create_socket(const char* host, int port) {
    int soc = socket(AF_INET, SOCK_STREAM, 0);
    struct addrinfo hints;
    struct addrinfo *result;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;

    int s = getaddrinfo(host, "80", &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    if(connect(soc, result->ai_addr, result->ai_addrlen) == -1) {
        printf("connect error");
        exit(EXIT_FAILURE);
    } else {
        printf("connected\n");
    }
    return soc;
}

/*
 * increases the amount of memory in a buffer by offset
 * 
 * @param buffer - the buffer to reallocate memory
 * @param offfset - the amount of memory to increase by
 */
void reallocate(Buffer* buffer, size_t offset) {
    buffer->data = realloc(buffer->data, buffer->length + offset);
    buffer->length = buffer->length + offset;
}

/*
 * reads the http response from the server from socket in to buffer
 * 
 * @param buf - the buffer to read the response into
 * @param soc - the socket which made the original http request 
 */
void read_resp_to_buf(Buffer* buf, int soc) {
    size_t full = 0;
    size_t read_amount = read(soc, buf->data + full, buf->length - full);
    while(read_amount != 0) {
        full += read_amount;

        if(full >= buf->length) {
            reallocate(buf, buf->length);
        }
        read_amount = read(soc, buf->data + full, buf->length - full);
    }
    buf->length = full;
    close(soc);
}

/*
 * creates a new buffer, allocates RESPONSE_BUF_SIZE amount of memory for the data,
 * and returns the buffer
 * 
 * @return Buffer - the buffer just created
 */
Buffer *create_buffer() {
    Buffer* result = malloc(sizeof(Buffer));
    result->length = RESPONSE_BUF_SIZE;
    result->data = malloc(RESPONSE_BUF_SIZE);
    return result;
}


/*
 * frees the memory allocated to buffer by malloc
 * 
 * @param buffer - the buffer which had memory allocated to it by malloc
 */
void clear_buffer(Buffer *buffer) {
    free(buffer->data);
    free(buffer);
}



/*
 * makes a get request to server host on port port at page page within range range 
 * and returns the socket opened to make the request
 * 
 * @param host - the host name of the server
 * @param page - the endpoint of the page of the requested file
 * @param range - the portion of the file we want to download
 * @param port - the port number of the server, 80 for http
 * @return int - file descriptor of the socket opened to make the request
 */
int make_get_request(char *host, char *page, const char *range, int port) {
    int soc = create_socket(host, port);
    char str[strlen(GET_REQUEST_FORMAT) + strlen(host) + strlen(page) + strlen(range) + 1];
    sprintf(str, GET_REQUEST_FORMAT, page, host, range);

    if(send(soc, str, strlen(str), 0) == -1) {
        fprintf(stderr, "send error! errno:%d\n", errno);
        exit(EXIT_FAILURE);
    }

    return soc;
}


/*
 * makes a head request to the server host at page page on port port
 * 
 * @param host - the host name of the server
 * @param page - the endpoint of the page
 * @param port - the port the server is on. 80 for http
 * @return int - the file descriptor of the socket opened
 */
int make_head_request(char *host, char *page, int port) {
    int soc = create_socket(host, port);
    char str[strlen(HEAD_REQUEST_FORMAT) + strlen(host) + strlen(page) + 1];
    sprintf(str, HEAD_REQUEST_FORMAT, page, host);
    if(send(soc, str, strlen(str), 0) == -1) {
        fprintf(stderr, "send error! errno:%d\n", errno);
        exit(EXIT_FAILURE);
    }

    return soc;
}



/*
 *
 */
Buffer* http_query(char *host, char *page, const char *range, int port) {
    Buffer* result = create_buffer();
    int soc = make_get_request(host, page, range, port);
    read_resp_to_buf(result, soc);
    return result;
}


/**
 * Separate the content from the header of an http request.
 * NOTE: returned string is an offset into the response, so
 * should not be freed by the user. Do not copy the data.
 * @param response - Buffer containing the HTTP response to separate 
 *                   content from
 * @return string response or NULL on failure (buffer is not HTTP response)
 */
char* http_get_content(Buffer *response) {

    char* header_end = strstr(response->data, "\r\n\r\n");

    if (header_end) {
        return header_end + 4;
    }
    else {
        return response->data;
    }
}


/**
 * Splits an HTTP url into host, page. On success, calls http_query
 * to execute the query against the url. 
 * @param url - Webpage url e.g. learn.canterbury.ac.nz/profile
 * @param range - The desired byte range of data to retrieve from the page
 * @return Buffer pointer holding raw string data or NULL on failure
 */
void split_url(const char *url, char *host, char *page) {
    char copy[BUF_SIZE];
    strncpy(copy, url, BUF_SIZE);
    char *index = strstr(copy, "/");

    if (index) {
        *index = '\0';
        strncpy(host, copy, BUF_SIZE);
        strncpy(page, index + 1, BUF_SIZE);

    } else {
        fprintf(stderr, "could not split url into host/page %s\n", url);
        exit(EXIT_FAILURE);
    }
}


Buffer *http_url(const char *url, const char *range) {
    char host[BUF_SIZE];
    char page[BUF_SIZE];
    split_url(url, host, page);
    return http_query(host, page, range, 80);
}


/*
 */
char *get_header_field(const char* field, char *header) {
    char *index = strstr(header, field);
    index = strstr(index, ": ");
    index += strlen(": ");
    (strstr(index, "\r"))[0] = '\0';
    return index;
}


/**
 * Makes a HEAD request to a given URL and gets the content length
 * Then determines max_chunk_size and number of split downloads needed
 * @param url   The URL of the resource to download
 * @param threads   The number of threads to be used for the download
 * @return int  The number of downloads needed satisfying max_chunk_size
 *              to download the resource
 */
int get_num_tasks(char *url, int threads) {
    printf("called get_num_tasks\n");
    Buffer* header = create_buffer();
    char host[BUF_SIZE];
    char page[BUF_SIZE];
    split_url(url, host, page);

    int soc = make_head_request(host, page, 80);
    read_resp_to_buf(header, soc);
    printf(header->data);

    int cont_len = atoi(get_header_field("Content-Length", header->data));

    if(threads == 0) {
        printf("threads cannot be zero");
        exit(EXIT_FAILURE);
    } else {
        //max_chunk_size = (cont_len / threads) + (cont_len % threads);
        max_chunk_size = (cont_len / threads) + 1;
    }
    clear_buffer(header);
    return threads;
}

/*
 * returns max chunk size
 * @ return int max chunk size
 */
int get_max_chunk_size() {
    return max_chunk_size;
}